export interface AutomodRule {
    when: WhenConditions;
    do: DoActions;
}
export interface WhenConditions {
    reddit?: {
        post?: PostTrigger;
        comment?: PostTrigger;
        submission?: PostTrigger;
    };
}
export interface DoActions {
    slack?: {
        alert: {
            channel: string;
            message: string;
        };
    };
}
interface PostTrigger {
    contains?: string | string[];
    regex?: string | string[] | RegExp | RegExp[];
}
export {};
