'use strict';
Object.defineProperty(exports, '__esModule', { value: true });
const main_1 = require('./services/main');
exports.AutoModService = main_1.default;
/**
 * Automatically loads all available services in this package
 * @param {ServiceBroker} broker
 */
function registerAllAutoModServices(broker) {
    broker.createService(main_1.default);
}
exports.default = registerAllAutoModServices;