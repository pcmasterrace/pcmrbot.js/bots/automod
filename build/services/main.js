'use strict';
Object.defineProperty(exports, '__esModule', { value: true });
const moleculer_1 = require('moleculer');
const jsyaml = require('js-yaml');
const Ajv = require('ajv');
const schema = require('../models/root');
class AutoModService extends moleculer_1.Service {
    constructor(broker) {
        super(broker);
        this.parseServiceSchema({
            name: 'automod',
            version: 3,
            dependencies: [
                {
                    name: 'accounts',
                    version: 3
                },
                {
                    name: 'reddit.post',
                    version: 3
                },
                {
                    name: 'reddit.wiki',
                    version: 3
                },
                {
                    name: 'reddit-rt.modlog',
                    version: 3
                },
                {
                    name: 'reddit-rt.post',
                    version: 3
                },
                {
                    name: 'slack.utilities',
                    version: 3
                },
                {
                    name: 'slack.web',
                    version: 3
                }
            ],
            settings: { channel: process.env.BOT_AUTOMOD_CHANNEL },
            actions: {
                updateConfig: {
                    name: 'config.update',
                    handler: this.updateConfig
                }
            },
            events: {
                // "v3.reddit-rt.modlog.removecomment": this.removePostHandler,
                // "v3.reddit-rt.modlog.removelink": this.removePostHandler,
                'v3.reddit-rt.modlog.wikirevise': this.wikiPageReviseHandler,
                'v3.reddit-rt.post.comment': this.postHandler,
                'v3.reddit-rt.post.submission': this.postHandler
            },
            created: this.serviceCreated,
            started: this.serviceStarted
        });
    }
    async postHandler(payload) {
        const isSubmission = payload.name.startsWith('t3');
        const postBody = isSubmission ? payload.selftext.toLowerCase() : payload.body.toLowerCase();
        for (let rule of this.automodConfig) {
            let matched = false;
            // When conditions
            if (rule.when.reddit) {
                if (rule.when.reddit.comment && !isSubmission) {
                    if (typeof rule.when.reddit.comment.contains === 'string') {
                        // Single string
                        if (postBody.indexOf(rule.when.reddit.comment.contains.toLowerCase()) > -1)
                            matched = true;
                    } else if (typeof rule.when.reddit.comment.contains === 'object') {
                        // List of strings
                        for (let phrase of rule.when.reddit.comment.contains) {
                            if (postBody.indexOf(phrase.toLowerCase()) > -1)
                                matched = true;
                        }
                    }
                }
                if (rule.when.reddit.submission && isSubmission) {
                    if (typeof rule.when.reddit.submission.contains === 'string') {
                        // Single string
                        if (postBody.indexOf(rule.when.reddit.submission.contains.toLowerCase()) > -1)
                            matched = true;
                    } else if (typeof rule.when.reddit.submission.contains === 'object') {
                        // List of strings
                        for (let phrase of rule.when.reddit.submission.contains) {
                            if (postBody.indexOf(phrase.toLowerCase()) > -1)
                                matched = true;
                        }
                    }
                }
                if (rule.when.reddit.post) {
                    if (typeof rule.when.reddit.post.contains === 'string') {
                        // Single string
                        if (postBody.indexOf(rule.when.reddit.post.contains.toLowerCase()) > -1)
                            matched = true;
                    } else if (typeof rule.when.reddit.post.contains === 'object') {
                        // List of strings
                        for (let phrase of rule.when.reddit.post.contains) {
                            if (postBody.indexOf(phrase.toLowerCase()) > -1)
                                matched = true;
                        }
                    }
                }
            }
            // Do actions
            if (matched) {
                if (rule.do.slack) {
                    if (rule.do.slack.alert) {
                        const blocks = await this.broker.call('v3.slack.utilities.generateSlackBlocks', { post: payload });
                        await this.broker.call('v3.slack.web.postMessage', {
                            channel: rule.do.slack.alert.channel,
                            blocks: [{
                                    type: 'section',
                                    text: {
                                        type: 'mrkdwn',
                                        text: rule.do.slack.alert.message
                                    }
                                }],
                            attachments: [{ blocks }]
                        });
                    }
                }
            }
        }
    }
    async removePostHandler(payload) {
    }
    async updateConfig(ctx) {
        const configPage = await ctx.call('v3.reddit.wiki.getPage', {
            subreddit: 'pcmasterrace',
            page: 'pcmrbotjs/automod'
        });
        // Get user information for Slack pings later.
        // If this fails, I've got bigger issues. 
        let user;
        try {
            user = await ctx.call('v3.accounts.getAssociatedAccount', {
                serviceType: 'reddit',
                userId: configPage.revision_by.name,
                getAccountType: 'slack'
            });
        } catch (err) {
            // Account not found errors should just have a generic message, not a ping
            if (err.code !== 404) {
                throw err;
            }
        }
        // Attempt to parse the configuration and store it in memory
        try {
            const config = jsyaml.loadAll(configPage.content_md);
            const results = this.validate(config);
            if (!results) {
                let message = '';
                for (let error of this.validate.errors) {
                    message += `${ error.keyword }: ${ error.dataPath } ${ error.message }.\n`;
                }
                throw new moleculer_1.Errors.MoleculerClientError(message, 422, 'INVALID_SCHEMA');
            }
            this.automodConfig = config;
            await ctx.call('v3.slack.web.postMessage', {
                channel: this.settings.channel,
                blocks: [{
                        type: 'section',
                        text: {
                            type: 'mrkdwn',
                            text: `${ user !== undefined ? '<@' + user.userId + '>: ' : '' }PCMRBot.js AutoMod configuration updated sucessfully.`
                        }
                    }]
            });
        } catch (err) {
            await ctx.call('v3.slack.web.postMessage', {
                channel: this.settings.channel,
                blocks: [{
                        type: 'section',
                        text: {
                            type: 'mrkdwn',
                            text: `${ user !== undefined ? '<@' + user.userId + '>: ' : '' }Error occurred while updating PCMRBot.js AutoMod configuration, details below`
                        }
                    }],
                attachments: [{
                        blocks: [{
                                type: 'section',
                                text: {
                                    type: 'mrkdwn',
                                    text: err.message
                                }
                            }],
                        color: '#FF0000'
                    }]
            });
        }
    }
    // This handler 
    async wikiPageReviseHandler(payload) {
        // Check to see if this is the PCMRBot.js AutoMod config page
        let page = /Page (\S+) edited/gim.exec(payload.details);
        if (page !== null && page[1] === 'pcmrbotjs/automod') {
            await this.broker.call('v3.automod.config.update');
        }
    }
    serviceCreated() {
        const ajv = new Ajv();
        this.validate = ajv.compile(schema);
    }
    async serviceStarted() {
        // Update the local service instance
        setTimeout(async () => await this.broker.call('v3.automod.config.update', {}, { nodeID: this.broker.nodeID }), 1000);
    }
}
exports.default = AutoModService;