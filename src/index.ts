import { ServiceBroker } from "moleculer";

import AutoModService from "@services/main";

/**
 * Automatically loads all available services in this package
 * @param {ServiceBroker} broker 
 */
export default function registerAllAutoModServices(broker: ServiceBroker): void {
    broker.createService(AutoModService)
}

export {
    AutoModService
}