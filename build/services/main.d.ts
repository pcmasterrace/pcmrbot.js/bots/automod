import { Service, Context } from "moleculer";
import * as Ajv from "ajv";
import { AutomodRule } from "../helpers/schemaTypings";
export default class AutoModService extends Service {
    protected automodConfig: AutomodRule[];
    protected validate: Ajv.ValidateFunction;
    constructor(broker: any);
    postHandler(payload: any): Promise<void>;
    removePostHandler(payload: any): Promise<void>;
    updateConfig(ctx: Context): Promise<void>;
    wikiPageReviseHandler(payload: any): Promise<void>;
    serviceCreated(): void;
    serviceStarted(): Promise<void>;
}
